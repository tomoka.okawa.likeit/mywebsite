package beans;

import java.io.Serializable;
import java.util.Date;

public class UserDataBeans implements Serializable {
	private int id;
	private String name;
	private String adress;
	private String loginId;
	private String password;
	private Date createDate;

	public UserDataBeans() {

	}

	public UserDataBeans(int id, String name, String adress, String loginId, String password,
			Date createDate) {
		this.id = id;
		this.name = name;
		this.adress = adress;
		this.loginId = loginId;
		this.password = password;
		this.createDate = createDate;

	}

	public UserDataBeans(int id, String loginId, String password, String adress) {
		this.id = id;
		this.loginId = loginId;
		this.password = password;
		this.adress = adress;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
