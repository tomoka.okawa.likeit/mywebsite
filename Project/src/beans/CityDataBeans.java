package beans;

import java.io.Serializable;

public class CityDataBeans implements Serializable {
	private int id;
	private String city_name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCity_name() {
		return city_name;
	}

	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}

}