package beans;

import java.io.Serializable;

public class ItemDataBeans implements Serializable {

	private int id;
	private String city_id;
	private String days;
	private String name;
	private String detail;
	private String price;
	private String fileName;

	private int cityId;
	private String cityName;

	public ItemDataBeans(){

	}

	ItemDataBeans(int id, String city_id, String days, String name, String detail, String price, String fileName) {
		this.id = id;
		this.city_id = city_id;
		this.days = days;
		this.name = name;
		this.detail = detail;
		this.price = price;
		this.fileName = fileName;
	}

	ItemDataBeans(String city_id, String days, String name, String detail, String price, String fileName) {
		this.city_id = city_id;
		this.days = days;
		this.name = name;
		this.detail = detail;
		this.price = price;
		this.fileName = fileName;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCity_id() {
		return city_id;
	}

	public void setCity_id(String city_id) {
		this.city_id = city_id;
	}

	public String getDays() {
		return days;
	}

	public void setDays(String days) {
		this.days = days;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}







}
