package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.CityDataBeans;
import beans.ItemDataBeans;
import dao.CityDao;
import dao.ItemDao;

/**
 * Servlet implementation class ItemMasterUpdate
 */
@WebServlet("/ItemMasterUpdate")
@MultipartConfig(location = "C:\\Users\\LIKEIT_STUDENT.DESKTOP-PJDC9PU.000\\Documents\\mywebsite\\Project\\WebContent\\image", maxFileSize = 10485760)
public class ItemMasterUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemMasterUpdate() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(true);
		try {
			String id = request.getParameter("id");

			ItemDao itemDao = new ItemDao();
			ItemDataBeans idb = itemDao.detailItem(id);

			request.setAttribute("idb", idb);

			CityDao cityDao = new CityDao();
			ArrayList<CityDataBeans> cityList = (ArrayList<CityDataBeans>) cityDao.findAll();

			request.setAttribute("citylist", cityList);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemmasterupdate.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession(true);
		try {

			String id = request.getParameter("id");
			String name = request.getParameter("name");
			String detail = request.getParameter("detail");
			String price = request.getParameter("price");
			String days = request.getParameter("days");
			String cityId = request.getParameter("city_id");
			Part part = request.getPart("fileName");

			String fileName = getFileName(part);
			part.write(fileName);

			ItemDao itemDao = new ItemDao();
			itemDao.updateItem(id, name, detail, price, days, cityId, fileName);

			response.sendRedirect("ItemMasterList");
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

	private String getFileName(Part part) {
		String name = null;
		for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
			if (dispotion.trim().startsWith("filename")) {
				name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
				name = name.substring(name.lastIndexOf("\\") + 1);
				break;
			}
		}
		return name;
	}

}
