package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDao;

/**
 * Servlet implementation class RegistConf
 */
@WebServlet("/RegistConf")
public class RegistConf extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegistConf() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {

			String inputName = request.getParameter("name");
			String inputAdress = request.getParameter("adress");
			String inputLoginId = request.getParameter("loginId");
			String inputPassword = request.getParameter("password");
			String inputPasswordConf = request.getParameter("passwordConf");

			UserDataBeans udb = new UserDataBeans();
			udb.setName(inputName);
			udb.setAdress(inputAdress);
			udb.setLoginId(inputLoginId);
			udb.setPassword(inputPassword);

			UserDao userDao = new UserDao();
			boolean isOK = userDao.isLoginId(inputLoginId);

			if (isOK) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
				dispatcher.forward(request, response);
				return;
			}

			if (!inputPassword.equals(inputPasswordConf)) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
				dispatcher.forward(request, response);
				return;
			}

			request.setAttribute("udb", udb);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registconf.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");

		}
	}
}
