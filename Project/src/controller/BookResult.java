package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BookDataBeans;
import beans.UserDataBeans;
import dao.BookDao;

/**
 * Servlet implementation class BookResult
 */
@WebServlet("/BookResult")
public class BookResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BookResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/bookresult.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);

		try {

			int itemId = Integer.parseInt(request.getParameter("item_id"));
			UserDataBeans userDataBeans = (UserDataBeans) session.getAttribute("udbInfo");

			BookDataBeans bdb = new BookDataBeans();
			bdb.setUserId(userDataBeans.getId());
			bdb.setItemId(itemId);

			BookDao bookDao = new BookDao();
			bookDao.insertBook(bdb);

			request.setAttribute("bdb", bdb);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/bookresult.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");

		}

	}
}
