package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

import base.DBManager;
import beans.CityDataBeans;

public class CityDao {

	public ArrayList<CityDataBeans> findAll() {
		Connection conn = null;
		ArrayList<CityDataBeans> cityList = new ArrayList<CityDataBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT * FROM m_city";
			Statement stmt = (Statement) conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				CityDataBeans cdb = new CityDataBeans();
				cdb.setId(rs.getInt("id"));
				cdb.setCity_name(rs.getString("city_name"));
				cityList.add(cdb);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return cityList;
	}

	public CityDataBeans cityGetId(String id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODOsここに処理を書いていく
			String sql = "SELECT * FROM m_city WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			CityDataBeans cdb = new CityDataBeans();
			if (rs.next()) {
				cdb.setId(rs.getInt("id"));
				cdb.setCity_name(rs.getString("city_name"));

			}

			return cdb;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

	}
}
