package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserDataBeans;

public class UserDao {

	public UserDataBeans findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM t_user WHERE login_id = ? and login_password = ?";

			String key = secretKey(password);
			System.out.println(key);

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, key);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int userId = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String passwordData = rs.getString("login_password");
			String adressData = rs.getString("adress");
			UserDataBeans userDataBeans = new UserDataBeans(userId, loginIdData, passwordData, adressData);
			return userDataBeans;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public static void insertUser(UserDataBeans udb) {
		Connection conn = null;
		PreparedStatement st = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "INSERT INTO t_user (name,adress,login_id,login_password,create_date) VALUES (?,?,?,?,?)";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, udb.getName());
			pStmt.setString(2, udb.getAdress());
			pStmt.setString(3, udb.getLoginId());
			pStmt.setString(4, secretKey(udb.getPassword()));
			pStmt.setTimestamp(5, new Timestamp(System.currentTimeMillis()));

			pStmt.executeUpdate();
			System.out.println("inserting user has been completed");

			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public UserDataBeans getUserDatabyUserId(int userId) {
		UserDataBeans udb = new UserDataBeans();
		Connection conn = null;
		PreparedStatement st = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			st = conn.prepareStatement("SELECT id,name,adress,login_id FROM t_user WHERE id =" + userId);
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				udb.setId(rs.getInt("id"));
				udb.setName(rs.getString("name"));
				udb.setAdress(rs.getString("adress"));
				udb.setLoginId(rs.getString("login_id"));
			}
			st.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return udb;
	}

	public boolean isLoginId(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM t_user WHERE login_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	public static String secretKey(String password) {
		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String key = DatatypeConverter.printHexBinary(bytes);

		return key;
	}

}
