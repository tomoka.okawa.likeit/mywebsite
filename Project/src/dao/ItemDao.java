package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

import base.DBManager;
import beans.ItemDataBeans;

public class ItemDao {

	public void insertItem(String name, String detail, String price, String days, String city_id, String file_name) {
		Connection conn = null;
		PreparedStatement st = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "INSERT INTO m_item (name,detail,price,days,city_id,file_name) VALUES (?,?,?,?,?,?)";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, detail);
			pStmt.setString(3, price);
			pStmt.setString(4, days);
			pStmt.setString(5, city_id);
			pStmt.setString(6, file_name);

			pStmt.executeUpdate();
			System.out.println("inserting item has been completed");

			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public ArrayList<ItemDataBeans> findAll() {
		Connection conn = null;
		ArrayList<ItemDataBeans> ItemDataList = new ArrayList<ItemDataBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT * FROM m_item";
			Statement stmt = (Statement) conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setCity_id(rs.getString("city_id"));
				idb.setName(rs.getString("name"));
				idb.setDetail(rs.getString("detail"));
				idb.setPrice(rs.getString("price"));
				idb.setDays(rs.getString("days"));
				idb.setFileName(rs.getString("file_name"));

				ItemDataList.add(idb);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return ItemDataList;
	}

	public ItemDataBeans detailItem(String id) {
		Connection conn = null;
		PreparedStatement st = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODOsここに処理を書いていく
			st = conn.prepareStatement(
					"SELECT m_item.id,"
							+ " m_item.name,"
							+ " m_item.detail,"
							+ " m_item.price,"
							+ " m_city.city_name,"
							+ " m_item.days,"
							+ " m_item.file_name"
							+ " FROM m_item"
							+ " JOIN m_city"
							+ " ON m_item.city_id = m_city.id"
							+ " WHERE m_item.id = ?");

			st.setString(1, id);

			ResultSet rs = st.executeQuery();

			ItemDataBeans idb = new ItemDataBeans();
			if (rs.next()) {
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setDetail(rs.getString("detail"));
				idb.setPrice(rs.getString("price"));
				idb.setCityName(rs.getString("city_name"));
				idb.setDays(rs.getString("days"));
				idb.setFileName(rs.getString("file_name"));

			}

			return idb;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

	}

	public ItemDataBeans deleteItem(String id, String name, String detail, String price, String days, String city_id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "DELETE FROM m_item WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

			int result = pStmt.executeUpdate();
			System.out.println(result);

			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public ItemDataBeans updateItem(String id, String name, String detail, String price, String days, String city_id,
			String file_name) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "UPDATE m_item SET name = ?,detail = ?,price = ?,days = ?,city_id = ?,file_name= ? WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, detail);
			pStmt.setString(3, price);
			pStmt.setString(4, days);
			pStmt.setString(5, city_id);
			pStmt.setString(6, file_name);
			pStmt.setString(7, id);

			int result = pStmt.executeUpdate();
			System.out.println(result);

			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public ArrayList<ItemDataBeans> getItemByCityID(String cityId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		ArrayList<ItemDataBeans> itemdataList = new ArrayList<ItemDataBeans>();

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_item WHERE city_id = ?");
			st.setString(1, cityId);

			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setCity_id(rs.getString("city_id"));
				idb.setName(rs.getString("name"));
				idb.setDetail(rs.getString("detail"));
				idb.setPrice(rs.getString("price"));
				idb.setDays(rs.getString("days"));
				idb.setFileName(rs.getString("file_name"));

				itemdataList.add(idb);
			}

			System.out.println("searching item by CityID has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		return itemdataList;
	}
}
