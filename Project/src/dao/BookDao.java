package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import base.DBManager;
import beans.BookDataBeans;

public class BookDao {
	public void insertBook(BookDataBeans bdb) {
		Connection conn = null;
		PreparedStatement st = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "INSERT INTO t_book (user_id,item_id,create_date) VALUES (?,?,?)";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, bdb.getUserId());
			pStmt.setInt(2, bdb.getItemId());
			pStmt.setTimestamp(3, new Timestamp(System.currentTimeMillis()));

			pStmt.executeUpdate();
			System.out.println("inserting booking has been completed");

			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public ArrayList<BookDataBeans> getBookDatabyUserId(int userId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		ArrayList<BookDataBeans> bdbList = new ArrayList<BookDataBeans>();
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT t_book.user_id,"
							+ "m_item.name,"
							+ " m_item.price,"
							+ " t_book.create_date"
							+ " FROM t_book"
							+ " JOIN m_item"
							+ " ON t_book.item_id = m_item.id"
							+ " WHERE t_book.user_id = ?");
			st.setInt(1, userId);

			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				BookDataBeans bdb = new BookDataBeans();
				bdb.setUserId(rs.getInt("user_id"));
				bdb.setName(rs.getString("name"));
				bdb.setPrice(rs.getInt("price"));
				bdb.setCreateDate(rs.getTimestamp("create_date"));

				bdbList.add(bdb);

			}

			System.out.println("searching BookDataBeans by ID has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
		return bdbList;
	}

}
