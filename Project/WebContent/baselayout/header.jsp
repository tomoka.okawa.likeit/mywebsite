<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<header class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
	<ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">

		<%
			boolean isLogin = session.getAttribute("udbInfo") != null ? true : false;
		%>

		<%
			if (isLogin) {
		%>
		<li class="nav-item"><a class="nav-link active" href="BookDetail"
			onclick="#">Reservation</a></li>
		<li class="nav-item"><a class="nav-link active" href="Logout"
			onclick="#">Logout</a></li>
		<%
			} else {
		%>
		<li class="nav-item"><a class="nav-link active" href="Login"
			onclick="#">Login</a></li>
		<li class="nav-item"><a class="nav-link active" href="Regist"
			onclick="#">Regist</a></li>
		<%
			}
		%>

		<c:if test="${udbInfo.id == 1}">
			<li class="nav-item"><a class="nav-link active"
				href="ItemMasterList" onclick="">Admin</a></li>
		</c:if>
	</ul>

</header>
</html>