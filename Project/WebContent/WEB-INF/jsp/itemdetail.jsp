<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>商品詳細</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">

</head>
<body>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />

	<!-- body -->
	<h1>
		<font size="6">商品詳細</font>
	</h1>
	<div class="container">
		<div class="row center">
			<div class="col-md-1"></div>
			<div class="col-md-9">
				<a class="btn btn-info" href="ItemList" role="button">戻る</a>

			</div>

			<div class="col-md-2">
				<a href="BookConf?id=${idb.id}" class="btn btn-info" role=button>予約</a>

			</div>
		</div>
	</div>



	<div class="album py-5 bg-light">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="card mb-4 shadow-sm">
						<img src="image/${idb.fileName}" width="100%" height="400">
					</div>
				</div>

				<div class="col-md-7">
					<h2>${idb.name}</h2>
					<h3>${idb.days}日間</h3>
					<h3>${idb.price}円</h3>
					<br>
					<hr style="border: 1px solid #17a2b8;"></hr>

					<h1></h1>
					<p>
						<font size="4">${idb.detail}</font>
					</p>

				</div>


			</div>
		</div>
	</div>

	<!-- footer -->
	<jsp:include page="/baselayout/footer.jsp" />

</body>
</html>
