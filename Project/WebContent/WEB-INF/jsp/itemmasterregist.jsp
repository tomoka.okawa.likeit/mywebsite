<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>商品マスタ登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">

</head>
<body>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />

	<h1>
		<font size="6">商品マスタ登録</font>
	</h1>
	<div class="container">
		<br></br>
		<form method="post" enctype="multipart/form-data" action="ItemMasterRegist">
			<div class="form-horizontal">
				<div class="form-group row">

					<label for="item_name" class="col-sm-2-form-label">商品名　</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="name">
					</div>
				</div>
			</div>


			<div class="form-group row">
				<label for="item_price" class="col-sm-2-form-label">商品価格</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="price">
				</div>
			</div>

			<div class="form-group row">
				<label for="item_price" class="col-sm-2-form-label">滞在都市</label>
				<div class="col-sm-10">

					<select name="city_id">
						<c:forEach var="city" items="${citylist}">
							<option value="${city.id}">${city.city_name}</option>
						</c:forEach>
					</select>


				</div>
			</div>

			<div class="form-group row">
				<label for="item_price" class="col-sm-2-form-label">滞在期間</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="days" value="">
				</div>
			</div>

			<div class="form-group row">

				<label>画像ファイル</label>
				<div class="col-sm-10">
					<input type="file" name="fileName"> <br>
				</div>
			</div>

			<div class="form-group row">
				<label for="item_detail" class="col-sm-2-form-label">商品詳細</label>
				<div class="col-sm-10">
					<textarea name=detail rows="40" cols="200"></textarea>
				</div>
			</div>

			<br></br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-5">
					<a href="ItemMasterList" class="btn btn-info" role="button"> 戻る
					</a>
				</div>

				<div class="col-sm-5">

					<button type="submit" class="btn btn-info">登録</button>

				</div>
			</div>
		</form>

	</div>

	<br></br>

	<!-- footer -->
	<jsp:include page="/baselayout/footer.jsp" />

</body>
</html>
