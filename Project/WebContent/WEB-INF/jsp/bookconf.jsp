<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>予約確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">

</head>
<body>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />

	<!-- body -->
	<h1>
		<font size="6">予約確認</font>
	</h1>

	<div class="album py-5 bg-light">
		<div class="container">
			<div class="row">

				<div class="col-md-5">
					<div class="card mb-4 shadow-sm">
						<img src="image/${idb.fileName}" width="100%" height="400">



					</div>
				</div>
				<div class="col-md-7">
					<h2>${idb.name}</h2>
					<h3>${idb.days}日間</h3>
					<h3>${idb.price}円</h3>
					<h1></h1>
					<h1></h1>
					<p>
						<font size="4">${idb.detail} </font>
					</p>

				</div>
			</div>
			<br></br>

			<hr style="border: 2px solid #17a2b8;"></hr>
			<form method="post" action="BookResult">
				<div class="card grey lighten-5">

					<input type="hidden" name="item_id" value="${idb.id}"> <input
						type="hidden" name="user_id" value="${udbInfo.id}">
					<table class="table table-style">
						<thead>
							<tr height="50">
								<th style="width: 70%">商品名</th>
								<th style="width: 30%">商品価格</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>${idb.name}</td>
								<td>${idb.price}円</td>

							</tr>

						</tbody>
					</table>
				</div>



				<div class="row">
					<div class="col-md-10">
						<a class="btn btn-info" href="ItemDetail" role="button">戻る</a>

					</div>
					<div class="col-md-2">

						<button type="submit" class="btn btn-info">予約する</button>

					</div>
				</div>
			</form>
		</div>
	</div>


	<!-- footer -->
	<jsp:include page="/baselayout/footer.jsp" />

</body>
</html>