<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>商品マスタ削除</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">
</head>

<body>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />

	<form method="post"enctype="multipart/form-data" action="ItemMasterDelete">

		<input type="hidden" name="id" value="${idb.id}">
		<input type="hidden" name="city_id" value="${idb.cityId}">
		<input type="hidden" name="detail" value="${idb.detail}">
		<input type="hidden" name="name" value="${idb.name}">
		<input type="hidden" name="price" value="${idb.price}">
		<input type="hidden" name="days" value="${idb.days}">

		<div class="container">
			<div class="delete-area">
				<div class="form-wrapper">
					<br></br>

					<p class="form-control-plaintext">
						<font size="5">${idb.name}</font>
					</p>
					<h1>
						<font size="5">を消去しますか？</font>
					</h1>


					<div class="row">
						<div class="col-sm-9">
							<a href="ItemMasterList" class="btn btn-info" role="button">
								キャンセル </a>
						</div>
						<div class="col-sm-3">
							<button type="submit" class="btn btn-info">削除</button>

						</div>
					</div>
				</div>
			</div>
		</div>


	</form>
	<!-- footer -->
	<jsp:include page="/baselayout/footer.jsp" />

</body>
</html>