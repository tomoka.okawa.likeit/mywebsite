<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>商品画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">

</head>
<body>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />

	<!-- body -->
	<div class="container">
		<br> <br> <br>
		<div class="row">
			<c:forEach var="item" items="${ItemDataList}">
				<div class="col-md-4">

					<div class="card mb-4 shadow-sm">
						<a href="ItemDetail?id=${item.id}"> <img
							src="image/${item.fileName}" width="100%" height="225"></a>
						<div class="card-body">
							<p class="card-text">${item.name}</p>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>



	<!-- footer -->

	<jsp:include page="/baselayout/footer.jsp" />

</body>
</html>
