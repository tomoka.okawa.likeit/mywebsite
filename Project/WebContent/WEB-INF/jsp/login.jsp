<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>ログイン</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">


</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />


	<div class="form-wrapper">
		<h1>
			<font size="6">ログイン</font>
		</h1>

		<form class="form-signin" action="Login" method="post">

			<c:if test="${errMsg != null}">
				<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>

			<div class="form-item">
				<label for="email"></label> <input type="text" name="loginId"
					required="required" placeholder="ログインID"></input>
			</div>
			<div class="form-item">
				<label for="password"></label> <input type="password"
					name="password" required="required" placeholder="パスワード"></input>
			</div>
			<div class="button-panel">
				<input type="submit" value="ログイン" class="button">

			</div>
		</form>

		<div class="form-footer">
			<p>
				<a href="Regist"><font size="4">新規登録</font></a>
			</p>

		</div>
	</div>

	<!-- footer -->
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>
