<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>登録完了</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">

</head>
<body>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />

	<!-- body -->
	<div class="form-wrapper">
		<h1></h1>
		<form>
			<div class="form-item">
				<label>名前</label> <label for="name"></label> <input type="text"
					value="${udb.name}" name="name" readonly>
			</div>

			<div class="form-item">
				<label>ログインID</label> <label for="loginId"></label> <input
					type="text" value="${udb.loginId}" name="loginId" readonly></input>
			</div>
			<h1>
				<font size="5">登録が完了しました</font>
			</h1>
		</form>
		<div class="button-panel">
			<a href="Login"><input type="submit" value="ログイン画面へ"
				class="button"></a>

		</div>

		<div class="form-footer"></div>
	</div>

	<!-- footer -->
	<jsp:include page="/baselayout/footer.jsp" />


</body>
</html>