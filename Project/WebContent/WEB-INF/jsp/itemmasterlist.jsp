<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>商品マスタリスト</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">

</head>
<body>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />


	<div class="container">
		<h1>
			<font size="6">商品マスタリスト</font>
		</h1>
		<br></br>

		<div class="create-button-area">
			<div class="row">
				<div class="col-sm-10"></div>
				<div class="col-sm-2">
					<a class="btn btn-primary" href="ItemMasterRegist">新規登録</a>
				</div>
			</div>
		</div>

		<div class="container table">
			<div class="row">
				<div class="col s12">
					<table class="table table-item">
						<thead>
							<tr height="50">
								<th width="60%">商品名</th>
								<th width="10%">商品価格</th>
								<th></th>
							</tr>
						</thead>

						<tbody>
							<c:forEach var="item" items="${ItemDataList}">
								<tr height="50">
									<td>${item.name}</td>
									<td>　${item.price}円</td>
									<td><a class="btn btn-info"
										href="ItemMasterDetail?id=${item.id}">詳細</a> <a
										class="btn btn-success" href="ItemMasterUpdate?id=${item.id}">更新</a>
										<a class="btn btn-danger"
										href="ItemMasterDelete?id=${item.id}">削除</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<!-- footer -->
	<jsp:include page="/baselayout/footer.jsp" />

</body>
</html>

