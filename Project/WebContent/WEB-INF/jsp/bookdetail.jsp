<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>予約確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">


</head>
<body>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />



	<h1>
		<font size="6">予約情報確認</font>
	</h1>
	<br></br>

	<div class="container">
		<div class="row center">
			<div class="col-md-10">
				<a class="btn btn-info" href="Index" role="button">戻る</a>

			</div>
		</div>
		<br></br>

		<div class="row">
			<div class="col-sm-4">
				<h3>予約者情報</h3>
			</div>
		</div>


		<div class="card grey lighten-5">
			<table class="table table-style">
				<thead>
					<tr height="50">
						<th style="width: 20%">ログインID</th>
						<th style="width: 20%">名前</th>
						<th style="width: 30%">住所</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>${udb.id}</td>
						<td>${udb.name}</td>
						<td>${udb.adress}</td>
					</tr>

				</tbody>
			</table>
		</div>
		<br></br>


		<div class="row">
			<div class="col-sm-4">
				<h3>予約情報</h3>
			</div>
		</div>
		<div class="card grey lighten-5">
			<table class="table table-style">
				<thead>
					<tr height="50">
						<th style="width: 20%">予約日時</th>
						<th style="width: 20%">商品価格</th>
						<th style="width: 30%">商品名</th>
					</tr>
				</thead>

				<tbody>
					<c:forEach var="bdb" items="${bdbList}">
						<tr>
							<td>${bdb.formatDate}</td>
							<td>${bdb.price}円</td>
							<td>${bdb.name}</td>
						</tr>
					</c:forEach>
				</tbody>

			</table>
		</div>
	</div>
	<br></br>
	<br></br>

	<!-- footer -->
	<jsp:include page="/baselayout/footer.jsp" />


</body>
</html>