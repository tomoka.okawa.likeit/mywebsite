<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>商品マスタ詳細</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">

</head>
<body>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />

	<div class="container">
		<h1>
			<font size="6">商品マスタ詳細</font>
		</h1>
		<br></br>

		<form>
			<div class="form-group row">
				<label for="item_name" class="col-sm-1-form-label">商品名 </label>
				<div class="col-sm-11">
					<p class="form-control-plaintext">${idb.name}</p>
				</div>
			</div>
		</form>

		<form>
			<div class="form-group row">
				<label for="item_price" class="col-sm-1-form-label">商品価格</label>
				<div class="col-sm-11">
					<p class="form-control-plaintext">${idb.price}円</p>
				</div>
			</div>
		</form>
		<form>
			<div class="form-group row">
				<label for="item_price" class="col-sm-1-form-label">滞在都市</label>
				<div class="col-sm-11">
					<p class="form-control-plaintext">${idb.cityName}</p>
				</div>
			</div>
		</form>
		<form>
			<div class="form-group row">
				<label for="item_price" class="col-sm-1-form-label">滞在期間</label>
				<div class="col-sm-11">
					<p class="form-control-plaintext">${idb.days}日間</p>

				</div>
			</div>
		</form>
		<form>
			<div class="form-group row">
				<label for="item_detail" class="col-sm-1-form-label">商品詳細</label>
				<div class="col-sm-11">
					<p class="form_detail">${idb.detail}</p>

				</div>
			</div>
		</form>
		<form>
			<div class="form-group row">

				<label>画像ファイル </label>
				<div class="col-sm-3">
					<div class="card mb-4 shadow-sm">
						<image src="image/${idb.fileName}" width="100%" height="225">
						</image>
					</div>
				</div>
			</div>
		</form>
		<a href="ItemMasterList" class="btn btn-info" role="button"> 戻る </a>

		<br></br>
	</div>
	<!-- footer -->
	<jsp:include page="/baselayout/footer.jsp" />


</body>
</html>