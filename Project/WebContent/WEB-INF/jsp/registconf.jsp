<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>入力内容確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">


</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />


	<div class="form-wrapper">
		<h1>
			<font size="6">入力内容確認</font>
		</h1>
		<form action="RegistResult" method="post">
			<c:if test="${errMsg != null}">
				<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>

			<div class="form-item">
				<label>名前</label> <label for="name"></label> <input type="text"
					value="${udb.name}" name="name" readonly>
			</div>
			<div class="form-item">
				<label>住所</label> <label for="adress"></label> <input type="text"
					value="${udb.adress}" name="adress" readonly></input>
			</div>
			<div class="form-item">
				<label>ログインID</label> <label for="loginId"></label> <input
					type="text" value="${udb.loginId}" name="loginId" readonly></input>
			</div>
			<div class="form-item">
				<label>パスワード</label> <label for="password"></label> <input
					type="password" value="${udb.password}" name="password" readonly></input>
			</div>
			<div class="form-item">
				<label>パスワード確認用</label> <label for="password"></label> <input
					type="password" value="${udb.password}" name="passwordConf"
					readonly></input>
			</div>


			<div class="button-panel">
				<input type="submit" value="登録" class="button">

			</div>
			<div class="col-xs-6">
				<a href="Regist"><font size="4">戻る</font></a>
			</div>

		</form>
		<div class="form-footer"></div>
	</div>

	<jsp:include page="/baselayout/footer.jsp" />


</body>
</html>
