<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>TOPページ</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">

</head>
<body>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />

	<!-- body -->

	<h1>イタリアツアーサイト</h1>

	<div class="album py-5 bg-light">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="image_area">
						<a href="ItemList?city_id=1"> <img src=image/roma.jpg
							width="100%" height="400">
							<p class="image text">ROMA</p>
						</a>
					</div>
				</div>


				<div class="col-md-6">
					<div class="image_area">
						<a href="ItemList?city_id=2"><img src=image/milano.jpg
							width="100%" height="400">
							<p class="image text">MILANO</p></a>



					</div>
				</div>

				<div class="col-md-6">
					<div class="image_area">
						<a href="ItemList?city_id=3"><img src=image/firenze.jpg
							width="100%" height="400">
							<p class="image text">FIRENZE</p></a>

					</div>
				</div>

				<div class="col-md-6">
					<div class="image_area">
						<a href="ItemList?city_id=4"><img src=image/napoli.jpg
							width="100%" height="400">
							<p class="image text">NAPOLI</p></a>


					</div>
				</div>

			</div>
		</div>
	</div>


	<!-- footer -->

	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>
