<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>新規登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">

</head>
<body>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />

	<!-- body -->
	<div class="form-wrapper">
		<h1>
			<font size="6">新規登録</font>
		</h1>
		<form action="RegistConf" method="post">

			<c:if test="${errMsg != null}">
				<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>

			<div class="form-item">
				<label for="name"></label> <input type="text" value="${udb.name}"
					name="name" required="required" placeholder="名前"></input>
			</div>
			<div class="form-item">
				<label for="adress"></label> <input type="text"
					value="${udb.adress}" name="adress" required="required"
					placeholder="住所"></input>
			</div>
			<div class="form-item">
				<label for="loginid"></label> <input type="text"
					value="${udb.loginId}" name="loginId" required="required"
					placeholder="ログインID"></input>
			</div>
			<div class="form-item">
				<label for="password"></label> <input type="password"
					value="${udb.password}" name="password" required="required"
					placeholder="パスワード"></input>
			</div>
			<div class="form-item">
				<label for="password"></label> <input type="password"
					value="${udb.passwordConf}" name="passwordConf" required="required"
					placeholder="パスワード確認用"></input>
			</div>


			<div class="button-panel">
				<input type="submit" value="確認" class="button">

			</div>
		</form>
		<div class="form-footer"></div>
	</div>

	<!-- footer -->
	<jsp:include page="/baselayout/footer.jsp" />


</body>
</html>