<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>ログアウト画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">

</head>
<body>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />

	<!-- body -->
	<div class="form-wrapper">

		<h1>
			<font size="6">ログアウトしました</font>
		</h1>

		<div class="button-panel">
			<a href="Login"><input type="submit" value="ログイン画面へ"
				class="button"> </a>
		</div>

	</div>

	<!-- footer -->
	<jsp:include page="/baselayout/footer.jsp" />

</body>
</html>