<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>商品マスタ更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">

</head>
<body>
	<!-- header -->
	<jsp:include page="/baselayout/header.jsp" />

	<div class="container">
		<h1>
			<font size="6">商品マスタ更新</font>
		</h1>
		<br></br>
		<div class="form-horizontal">
			<form method="post" enctype="multipart/form-data"
				action="ItemMasterUpdate">
				<input type="hidden" name="id" value="${idb.id}">


				<div class="form-group row">
					<label for="item_name" class="col-sm-2-form-label">商品名　</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="name"
							value="${idb.name}">
					</div>
				</div>



				<div class="form-group row">
					<label for="item_price" class="col-sm-2-form-label">商品価格</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="price"
							value="${idb.price}　円">
					</div>
				</div>

				<div class="form-group row">
					<label for="item_price" class="col-sm-2-form-label">滞在都市</label>
					<div class="col-sm-10">
						<select name="city_id">
							<c:forEach var="city" items="${citylist}">
								<option value="${city.id}">${city.city_name}</option>
							</c:forEach>
						</select>
					</div>
				</div>

				<div class="form-group row">
					<label for="item_price" class="col-sm-2-form-label">滞在期間</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="days"
							value="${idb.days}　日間">
					</div>
				</div>

				<div class="form-group row">
					<label>画像ファイル</label>
					<div class="col-sm-10">
						<input type="file" name="fileName">
					</div>
				</div>

				<div class="form-group row">
					<label for="item_detail" class="col-sm-2-form-label">商品詳細</label>
					<div class="col-sm-10">
						<textarea name=detail rows="40" cols="200">${idb.detail}</textarea>
					</div>
				</div>





				<br></br>
				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-5">
						<a href="ItemMasterList" class="btn btn-info" role="button">
							戻る </a>
					</div>

					<div class="col-sm-5">

						<button type="submit" class="btn btn-info">更新</button>

					</div>
				</div>
			</form>
		</div>
	</div>

	<br></br>
	<!-- footer -->
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>